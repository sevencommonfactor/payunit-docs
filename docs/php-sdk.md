# PHP SDK

### Pre-requisites

- PHP = 7.2 && PHP>=7.4+ (Not working with PHP 7.3)
- Create an account on Paynit as a merchant. Click[ to create an account.](https://app.payunit.net/#/ " to create an account.")
- Get the merchant Api User, merchant API key, and merchant API Password from the merchant dashboard under the credentials section


![Dashboard image](images/dashboardv2.png)

Get the merchant Api User, merchant API key, and merchant API Password from the merchant dashboard under the credentials section

### Installation

```php
composer require sevengps/payunit
```

### Using the SDK

To conveniently use Payunit, Add the Payunit Namespace in your desired controller.

```php
 use SevenGps\PayUnit
```

Create a new instance of the Payunit class and pass in all the required parameters.

```php
$myPayment = new PayUnit(
      "api_key",
      "api_password",
      "api_username",
      "returnUrl",
      "notifyUrl",
      "mode",
      "description",
      "purchaseRef",
      "currency",
      "name",
      "transactionId"
    );
```

Call the MakePayment method to make a payment

```php
 $myPayment->makePayment("total_amount");
```

## Configuration

- To Test Visa/Master Card in the Sandbox environment use the following information :
	- Card Number: **4242 4224 2424 2424** or **2223 0000 4840 0011**
- To test PayPal in the Sandbox environment use the following credential 
	- Email: **sb-hf17g4673731@business.example.com**
	password: **ehQ5_)dA**

|  Attribute |  Description |  Mandatory |
| ------------ | ------------ | ------------ |
|  api_username | Merchant Api Username gotten from merchant dashboard under credentials section  | yes  |
| api_password  | Merchant Api Password  gotten from merchant dashboard under credentials section  |  yes |
| api_key  |  Merchant Api Key gotten from merchant dashboard under credentials section | yes  |
| mode  | The current mode operation. Can either be **“test”** when testing in sandbox or **“live”** when ready for production.  | yes  |
|  return_url |  The url or endpoint to be called upon payment completion |  yes |
|  notify_url |  The url or endpoint to submit a transaction response to. This url or endpoint accepts a POST request of format: { “transaction_id:”6465464”, “transaction_amount”:”5000″, “transaction_status”:”SUCCESS”, “error”:null, “message”:”Transaction of 5000 XAF was successfully completed” } | no  |
| purchaseRef  |  A reference which you can give from your end to a particular transaction |  no |
|  total_amount | The amount or price of the product/products to be paid for  | yes  |
| description  | A description you can give to this type of  transaction  |  no |
|  name | Name of the merchant  |  yes |
| currency  |  Can be XAF, USD or any currently supported currency |  yes |
|  transactionId | id that uniquely identifies the transaction and I must be unique for a transaction. This id should be alpha numeric and less than 20 characters  | yes  |

If everything is put in place, a call to the PayUnit API will be made and if the request turns out successful, you will be redirected to the PayUnit payment interface. The interface is shown below.


![Hosted Payment image](images/hostedpagesv2.png)

### Demo 

You can check out this demo at https://gitlab.com/sevencommonfactor/payunit-php-demo/-/tree/dev to see payunit in action.
Watch demo video here https://youtu.be/9q6ZgDJH23k

### Recommendations

Please for security reasons make sure you read your Api key, Api password and Api user from an environment file

