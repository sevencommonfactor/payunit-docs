# REST-API
### Pre-requisites 
- Create an account on Payunit as a merchant. Click [to create an account.](https://app.payunit.net/#/ "to create an account.")
- Get the merchant Api User, merchant API key, and merchant API Password from the merchant dashboard under the credentials section



![Dashboard image](images/dashboardv2.png)



The PayUnit REST API provides a restful interface for developers to make direct API calls to the PayUnit payment aggregator.

The PayUnit REST-API requires the developer to have a server setup. This server is expected to implement and expose a notify endpoint which will receive and handle the payment transaction responses submitted by PayUnit. PayUnit expects that this endpoint receives a POST request with the payload shown below.
```json
{
	"transaction_id:"6465464",
	"transaction_amount":"5000",
	"transaction_status":"SUCCESS",
	"error":null,
	"message":"Transaction of 5000 XAF was successfully completed"
}
```
### Making the API calls
**Base URL :** https://app.payunit.net/api

**Http Headers :**

Specifications HTTP headers for all the API calls

| Http header | Description |
| ------------ | ------------ |
| Content-Type  |Content type supported is :  application/json   |
| Authorization  | Http basic Auth encoded in Base 64 and formatted as follow : Basic Base64(apiUser:apiPassword)  |
|  x-api-key |Your api key found in your PayUnit Dashboard   |
| mode  | The current mode operation. Can either be **“test”** when testing in sandbox or **“live”** when ready for production.  |

1. Request to initialization a transaction on PayUnit
- End point: {BASE_URL}/gateway/initialize
- Request method: POST

**Request body :**

|  Parameter | Mandatory  | Description  |
| ------------ | ------------ | ------------ |
|total_amount	   |  yes | The amount to be paid  |
|  transaction_id | yes  | This transaction should be unique per request and it should be a string. This id should be alphanumeric and less than 20 characters  |
| currency  |  yes | The currency you want to use: XAF   |
|  return_url | yes  | The url or endpoint to be called upon payment completion   |
|  notify_url | no  | Payment result notification to your server   |
| name  | no  |  Name of the merchant  |
|  description |  no | A description you can give to this type of  transaction   |
|  purchaseRef | no  |  A reference that you can give from your end to a particular transaction |

**Example:**

```
curl -X POST \
   '{BASE_URL}/gateway/initialize' \
 	-H 'Authorization: Basic cGF5dW5pdF9UU5LTg1MjMtZTEwZDZmMDg1ODk3' \
	-H 'Content-Type: application/json' \
	-H 'x-api-key: your_api_key' \
	-H 'mode: test' \
	-d '{
	"total_amount" : "amount_to_be_pay",
	"currency" : "XAF",
	"transaction_id" : "unique_id_in_your_system",
	"return_url" : "https://my.website.com/payunit/return",
	"description" : "your_description", 
	"purchaseRef" : "any_reference_number", 
	"name": "your_app_name" 
	}'

```

**Response body :**

|  Parameter | Description  |
| ------------ | ------------ |
| t_id  |  The transaction id that has been encrypted |
| t_sum  |  The amount that has been encrypted |
| t_url  |  The return url that has been encrypted |
|  transaction_id | This transaction should be unique per request and it should be a string. This id should be alphanumeric and less than 20 characters  |
|  transaction_url  | The return url that takes a user to the payment platform  |

**Example:**
```json
{
    "status": "SUCCESS",
    "statusCode": 200,
    "message": "Transaction Created.",
    "data": {
       "t_id": "MTNkOWQ0NmE4ZTE2ZGFhMmE4Nzg0ZDNlYWVkODBiOGQwMDc4MzM3MGYzZGM1ZGRj",
        "t_sum": "MTYxMjUwMjE4NDBkMjc1YmZhNDVhM2FiNWM4NGVkNTQzNjk5Y2E==",
        "t_url": "ZWNlZGNhY2EzOWFmY2JhYjllMGE3NTVhZTU2ZmYyZjM4MGEyZTliNGI5NWIyMWEy",
        "transaction_url": "https://hostedpages.payunit.net/#/hostedPayment/payment/?t_id=ZDJhZmNhMzJhMWY5NGJlZjhmY2E5MDRlYTUzMjk9370",
        "transaction_id": "5222fg525rtrtrt22"
    }
}

```

**2- Making payments on PayUnit**

**2-1 Using Hosted Pages**

Use the link available in the transaction_url response to be redirected to the PayUnit payment interface. The interface is shown below.



![Hosted Payment Image](images/hostedpagesv2.png)

**2-2 Continuous using Rest API** ( Working only for the following PSP : Orange , Mtn, Express Union)

**2-2-1 Get all PSP on PayUnit**

- End point: {BASE_URL}/gateway/gateways?t_url=””&t_id=””&t_sum=””
- Request method: GET

**Request parameter :**

|  Parameter | Mandatory  | Description  |
| ------------ | ------------ | ------------ |
|  t_id |  yes | The transaction id that has been encrypted  |
|  t_sum |  yes | The amount that has been encrypted  |
|  t_url |  yes | The return url that has been encrypted  |

**Example:**

```json
curl -X GET \
	'{BASE_URL}/gateway/gateways?t_url=tyty*****&t_id=ytyy*****&t_sum=ytyty*****' \
	-H 'Content-Type: application/json' \
    -H 'Authorization: Basic cGF5dW5pdF9UU5LTg1MjMtZTEwZDZmMDg1ODk3' \
    -H 'x-api-key: your_api_key' \
    -H 'mode: test' 

```

**Response body :**

|  Parameter | Description  |
| ------------ | ------------ |
| statusCode  | The statusCode of the request: -20x for success,   -400 or 500 for failed   |
| message  | The message sent back by the system  |
|  created_at |  creation date |
|  updated_at |  updated date |
| privider_id  |  id of the provider |
|  provider_name |  name of the provider |
| privider_logo  |  logo of the provider |
|  provider_short_tag |  provider short tag |
|  provider_status | status of the provider  |

**Example**

```json
{
    "status": "SUCCESS",
    "statusCode": 200,
    "message": "ok",
    "data": [
        {
            "create_time": "2020-10-12 15:22:57",
            "update_time": "2020-10-12 15:22:57",
            "provider_id": "3",
            "provider_name": "MTN MOMO",
            "provider_logo": "https://core.payunit.net/resources/flags/mtn.png",
            "provider_short_tag": "mtnmomo",
            "provider_status": "ACTIVE",
            "delete_time": null,
            "service_accounts_account_id": null,
            "service_accounts_users_user_id": null,
            "providers_provider_id": null
        },
        {
            "create_time": "2020-10-12 15:26:38",
            "update_time": "2020-11-05 06:14:05",
            "provider_id": "5",
            "provider_name": "EXPRESS UNION MOBILE",
            "provider_logo": "https://core.payunit.net/resources/flags/eu.png",
            "provider_short_tag": "eu",
            "provider_status": "ACTIVE",
            "delete_time": null,
            "service_accounts_account_id": null,
            "service_accounts_users_user_id": null,
            "providers_provider_id": null
        },
        
    ]
}
```

**2-2-2 Make payment on PayUnit**

- End point: {BASE_URL}/gateway/makepayment
- Request method: POST

**Request Body :**

| Parameter  | Mandatory  | Description  |
| ------------ | ------------ | ------------ |
|  gateway |  yes |  provider short tag |
|  amount |  yes | The amount to be pay   |
|  transaction_id | yes  | This transaction should be unique per request   |
|  notify_url |  no | Payment result notification to your server   |
|  name | no  | Name of the merchant   |
| phone_number  |  yes |  The phone number of you customer  |
|  currency | yes  | The currency you want to use: XAF   |
|  paymentType | yes  | Payment Type   |

**Example**

```json
curl -X POST \
   '{BASE_URL}/gateway/makepayment' \
 	-H 'Authorization: Basic  cGF5dW5pdF9UU5LTg1MjMtZTEwZDZmMDg1ODk3' \
	-H 'Content-Type: application/json' \
	-H 'x-api-key: your_api_key' \
	-H 'mode: test' \
	-d '{
	"gateway" : "mtnmomo",
	"amount" : 15000,
	"transaction_id" : "unique_id_in_your_system",
	"return_url" : "https://my.website.com/payunit/return",
	"phone_number" : "655487***", 
	"currency" : "XAF", 
    "paymentType": "button"
	"name": "your_app_name" 
	}'
```

**Response body for Mtn Psp :**

|  Parameter | Description  |
| ------------ | ------------ |
| payment_ref  | Reference of the Payment  |
|  transaction_id |  Id of the transaction |
|  pay_token |  Payment token |

**Example**

```json
{
    "status": "SUCCESS",
    "statusCode": 200,
    "message": "Please confirm the Transaction on your mobile by dialing *126#",
    "data": {
        "payment_ref": "b725d5e6-4612-4e33-89c6-cf3188b57c80",
        "transaction_id": "5222fg525rtrtrt22",
        "pay_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSMjU2In0.eyJjbGllbn*****"
    }
}
```

**Response body for Orange Psp :**

| Parameter  |  Description |
| ------------ | ------------ |
|  auth-token | authorization token  |
|  x-token | x token  |
|  transaction_id |  Id of the transaction |
|  paytoken |  Payment token |

**Example**

```json
{
    status: "PENDING",
    statusCode: 200,
    message: "Merchant payment successfully initiated. Enter your PIN on your mobile to confirm.",
    data: {
        auth-token: "b725d5e6-4612-4e33-89c6-cf3188b57c80",
        transaction_id: "5222fg525rtrtrt22",
        init: "Payment done.The customer will have to confirm the payment by entering his PIN and then you will receive an SMS. Thank you for using Orange Money service."
        x-token:"hjhhjhkjkkjhkjh",
        message: "Merchant payment successfully initiated"
        paytoken: "eyJ0eXAiOiJKV1QiLCJI6ImU3N2E3NmQ5LTg4MTEtNDhlZC******"
        redirect_url: "https://core.payunit.net/gateway/callback/orange/MTYyNjE4NDA0MDI3OQ=="
    }
}
```

**3. Getting the payment transaction status on PayUnit**

3-1 Get Transaction status for Mtn Psp
- End point: 
{BASE_URL}/gateway/paymentstatus/{gateway}/{transaction_id}pay_token=${pay_token}&payment_ref=${payment_ref}
- Request method: GET

**Request parameter :**

|  Parameter |  Mandatory |  Description |
| ------------ | ------------ | ------------ |
|  gateway |  yes | provider short tag  |
| payment_ref  | yes  |  Reference of the Payment   |
|  transaction_id | yes  | Transaction id  |
|  pay_token |  yes |  Payment token |

**Example**

```json
curl -X GET \
	'{BASE_URL}/gateway/paymentstatus/mtnmomo/Sp2021155212553819?pay_token=P4KfPXly_gWtHWlCsDkJVRV6TVVzdsR5w3Yhz22vQ&payment_ref=ac8b3aa2-01b6-495a-ab06-eee267c7bbdd' \
	-H 'Content-Type: application/json' \
    -H 'x-api-key: your_api_key' \
    -H 'Authorization: Basic cGF5dW5pdF9UU5LTg1MjMtZTEwZDZmMDg1ODk3' \
    -H 'mode: test' 

```

**Response body :**

|  Parameter | Description  |
| ------------ | ------------ |
|  transaction_id | Id of the transaction  |
|  transactions | the transaction  |
| status  | Transaction Status: -SUCCESSFUL for Payment successful,  – FAILED for Payment failed  |
| callback_url  | The url or endpoint to be called upon payment completion  |
|  transaction_amount | Amount of the transactions  |
|  statusCode | The statusCode of the request:  – 20x for success,  – 400 or 500 for failed  |

**Response Example:**

```json
{
    status: "SUCCESSFUL",
    statusCode: 200,
    message: "Your transaction completed successfully",
    data: {
            transaction_id: "5222fg525rtrtrt22",
            callback_url:"http://localhost:4200/home/gateway/initialize"
    }
}
```

**3-2 Get Transaction status for Orange Psp**

- End point: 
{BASE_URL}/gateway/paymentstatus/{gateway}/{transaction_id}?paytoken=${payToken}&auth-token=${authToken}&x-token=${Xtoken}
- Request method: GET

**Request parameter :**

|  Parameter | Mandatory  |  Description |
| ------------ | ------------ | ------------ |
|  gateway | yes  | provider short tag  |
|  transaction_id | yes  | Transaction id  |
|  paytoken |  yes  | Payment token  |
|  x-token | yes  | x-token  |
| auth-token  |  yes | authorization token  |

**Example**

```json
curl -X GET \
	'{BASE_URL}/gateway/paymentstatus/orange/Sp2021155212553819?paytoken=P4KfPXly_gWtHWlCsDkJVRV6TVVzdsR5w3Yhz22vQ&auth-token=ac8b3aa2-01b6-495a-ab06-eee267c7bbdd&x-token=jhlsjsjhdsjhdskjkjk'\
	-H 'Content-Type: application/json' \
    -H 'x-api-key: your_api_key' \
    -H 'Authorization: Basic cGF5dW5pdF9UU5LTg1MjMtZTEwZDZmMDg1ODk3' \
    -H 'mode: test' 

```

**Response body :**

|  Parameter | Description  |
| ------------ | ------------ |
|  transaction_id | Id of the transaction  |
|  transactions | the transaction  |
|  status |  Transaction Status: -SUCCESSFUL for Payment successful,  – FAILED for Payment failed |
|  callback_url |  The url or endpoint to be called upon payment completion |
|  statusCode | The statusCode of the request: – 20x for success,  – 400 or 500 for failed  |

https://hostedpages.payunit.net/#/

**Response Example:**

```json
{
    status: "SUCCESSFULL",
    statusCode: 200,
    message: "Paiement de SEVEN ACADEMY reussi ..........",
    data: {
            transaction_id: "5222fg525rtrtrt22",
            transaction_amount: 200
            callback:"http://localhost:4200/home/gateway/initialize"
    }
}
```








  



