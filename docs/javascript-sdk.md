# Javascript SDK
### Pre-requisites
- Create an account on Payunit as a merchant. Click [to create an account.](https://app.payunit.net/#/ "to create an account.")
- Get the merchant Api User, merchant API key, and merchant API Password from the merchant dashboard under the credentials section.

![Dashboard image](images/dashboardv2.png)

### Installation and Usage of the SDK
There are two ways by which the SDK can be used.
- Using the Payunit npm package or
- Using the Payunit JavaScript CDN

1. **Using the npm package**

## Angular

#### Installation
```javascript
 npm install ngx-payunit

```

```javascript
import { NgxPayunitModule } from 'ngx-payunit';
@NgModule({
  declarations: [AppComponent],
  imports: [
    NgxPayunitModule
  ],
 })
```

#### Usage


```javascript
import { Component, OnInit } from '@angular/core';
import { NgxPayunitComponent } from 'ngx-payunit';
 
@Component({
 templateUrl: './your.component.html',
})
export class YourComponent implements OnInit {
 private payunit: NgxPayunitComponent;
 constructor() {
    this.payunit = new NgxPayunitComponent();
  }
 ngOnInit(): void {
   this.initConfig();
  }
 
 
 private initConfig(): void {
    let config = {
      apiUsername: '',
      apiPassword: '',
      x_api_key: '',
      mode: '', 
    };
    let data = {
      return_url: '',
      notify_url: '',
      description: '',
      purchaseRef: '',
      total_amount: '',
      currency: '',
      transaction_id: '',
    };
    this.payunit.config(config);
    this.payunit.payload(data);
 }
  makePayment() {
    this.payunit.pay();
  }
}
```

```html
<ngx-payunit></ngx-payunit>
   <button (click)="makePayment()">Pay with payunit</button>
```

#### Demo

You can find a Angular demo app using payunit on :

```
https://gitlab.com/sevencommonfactor/payunit-angular-demo/-/tree/dev
```

## VueJs

### Installation

```javascript
  npm install payunitjs
```

#### Usage

```javascript
<template>
  <div>
    <button id="payunit-pay">Pay with payunit</button>
  </div>
</template>
<script>
import { PayUnit } from "payunitjs";
export default {
  mounted() {
    PayUnit(
      {
        apiUsername: "",
        apiPassword: "",
        x_api_key: "",
        mode: "",
      },
      {
        return_url: "",
        notify_url: " ",
        description: "",
        purchaseRef: "",
        total_amount: "",
        name: "",
        currency: "",
      }
    );
  },
};
</script>

```

#### Demo

You can find a Vuejs demo app using payunit on :
```
https://gitlab.com/sevencommonfactor/payunit-vuejs-demo/-/tree/dev
```

## React

#### Installation

```javascript
  npm install payunitjs
```

#### Usage

```javascript
import React , {Component}  from 'react'
import { PayUnit } from "payunitjs"
export default class YourComponent extends Component {
    
    componentDidMount() {
        PayUnit(
            {
              apiUsername: "",
              apiPassword: "",
              x_api_key: "",
              mode: "",
            },
            {
              return_url: "",
              notify_url: " ",
              description: "",
              purchaseRef: "",
              total_amount: "",
              name: "",
              currency: "",
            }
          );
    }
   render() {
      return (
          <div>
             <button id ="payunit-pay"> Pay  with payunit </button>
          </div>
        )
    }
}

```

#### Demo

You can find a Reactjs demo app using payunit on :

```
https://gitlab.com/sevencommonfactor/payunit-reactjs-demo/-/tree/dev
```

### Configuration
- To Test Visa/Master Card in the Sandbox environment use the following information
	- Card Number: **4242 4224 2424 2424** or **2223 0000 4840 0011**
- To test PayPal in the Sandbox environment use the following credential 
	- Email: **sb-hf17g4673731@business.example.com**
	password: **ehQ5_)dA**

|  Attribute | Description  |  Mandatory |
| ------------ | ------------ | ------------ |
|  apiUsername | Merchant Api Username gotten from merchant dashboard under credentials section  |  yes |
| apiPassword  |  Merchant Api Password  gotten from merchant dashboard under credentials section |  yes |
|  x_api_key | Merchant Api Key gotten from merchant dashboard under credentials section  | yes  |
| mode  | The current mode operation. Can either be **“test”** when testing in sandbox or **“live”** when ready for production.  | yes  |
| return_url  | The url or endpoint to be called upon payment completion  | yes  |
| notify_url  | The url or endpoint to submit a transaction response to. This url or endpoint accepts a POST request of format: { “transaction_id:”6465464”, “transaction_amount”:”5000″, “transaction_status”:”SUCCESS”, “error”:null, “message”:”Transaction of 5000 XAF was successfully completed” }  |  no |
|  purchaseRef | A reference which you can give from your end to a particular transaction  | no  |
|  total_amount | The amount or price of the product/products to be paid for.  | yes  |
|  description | A description you can give to this type of  transaction  | no  |
|  Name |  Name of the merchant | yes  |
| currency  | Can be XAF, USD or any currently supported currency  | yes  |
| transaction_id  |  id that uniquely identifies the transaction and I must be unique for a transaction. This id has to me at most 20 characters. An example can be of this form 465sdfsdf464asdfsa | yes  |


**2.  Using the SDK from its official CDN**
- Add a script tag and set the value of the type attribute to module i.e type = “module”.
- Import the Payunit function from the CDN. In the snippet below, version 1.0.4 of the sdk is used which is currently the latest stable release.
- Within the script tag, call the function as shown below and make sure you pass in all the required parameters.

### Usage

```javascript
<script type="module">
  import { PayUnit } from "https://cdn.jsdelivr.net/npm/payunitjs@1.0.4/dist/payunit.min.js";
  PayUnit(
    {
      apiUsername: "",
      apiPassword: "",
      x_api_key: "",
      mode: "",
    },
    {
      return_url: " ",
      notify_url: " ",
      description: "",
      purchaseRef: "",
      total_amount: "",
      name: "",
      currency: "XAF",
      transaction_id: '',
    }
  );
</script>
```

```javascript
  <button id = "payunit-pay"> Pay </button>
```


![Hosted Payment image](images/hostedpagesv2.png)

With everything in place, the payment is then processed and transaction result is submitted to the notify_url that was specified. In case the notify_url was not specified, the transaction result is sent as encoded query parameters to the return_url.

### Recommendations

For security concerns, make sure you read your Api key, Api password and Api user from a config file.